#!/bin/sh

cd "`dirname $0`"
cd ../packages

echo "#!/usr/bin/env python" > __init__.py
for i in *.py
do
  if [ ! "$i" = "__init__.py" ] ; then 
      echo "from `basename $i .py` import *" >> __init__.py
  fi
done
