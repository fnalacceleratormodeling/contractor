#!/usr/bin/env python

from nodes import Files
from utils import *

ENV_SET=1
ENV_PREPEND=2
ENV_APPEND=3

class Environment_mod:
    def __init__(self,var,item,action=ENV_PREPEND,delimiter=':'):
        if ( (action != ENV_SET) and 
            (action != ENV_PREPEND) and 
            (action != ENV_APPEND)):
            raise RuntimeError, \
                'Environment_mod: action must be either ENV_SET, ENV_PREPEND, or ENV_APPEND'
        self.var = var
        self.item = item
        self.action = action
        self.delimiter = delimiter
        
    def current_env_modify(self):
        if self.action == ENV_SET:
            os.environ[self.var] = self.item
        else:
            if  os.environ.has_key(self.var):
                if self.action == ENV_PREPEND:
                    new_value = self.item + self.delimiter + os.environ[self.var]
                elif self.action == ENV_APPEND:
                    new_value = os.environ[self.var] + self.delimiter + self.item
                else:
                    raise RuntimeError, 'impossible has happened'
            else:
                new_value = self.item
            os.environ[self.var] = new_value
 
class Environment_mod_node(Environment_mod):
    def __init__(self,root,owner,var,item,action=ENV_PREPEND,delimiter=':',
                 index=None):
        Environment_mod.__init__(self,var,item,action,delimiter)
        self.root = root
        self.owner = owner
        if index != None:
            index_str = "%02d" % index
        else:
            index_str = ""
        base_filename = os.path.join(self.root.get_var('setup_dir'),
            '%s-%s%s' % (self.owner,self.var.lower(),index_str))
        self.ctct_file = base_filename + '.ctct'
        self.sh_file = base_filename + '.sh'
        self.csh_file = base_filename + '.csh'
        self.node = Files(owner + '-' + var + index_str,
                          self.filenames(),self.create_files)
    
    def filenames(self):
        return [self.ctct_file, self.sh_file, self.csh_file]

    def create_files(self):
        ctct_file = open(self.ctct_file,'w')
        ctct_file.write("%s\n" % self.var)
        ctct_file.write("%s\n" % self.item.replace('\n','CONTRACTOR_NEWLINE'))
        ctct_file.write("%d\n" % self.action)
        ctct_file.write("%s\n" % self.delimiter)
        ctct_file.close()
        
        self.current_env_modify()

        sh_file = open(self.sh_file,"w")
        csh_file = open(self.csh_file,"w")
        if self.action == ENV_SET:
            new_value_sh = self.item
            new_value_csh = self.item
        else:
            if self.action == ENV_PREPEND:
                new_value_sh = self.item + self.delimiter + '$' + self.var
                new_value_csh = self.item + self.delimiter + '${' + self.var + '}'
            elif self.action == ENV_APPEND:
                new_value_sh = '$' + self.var + self.delimiter + self.item
                new_value_csh = '${' + self.var + '}'+ self.delimiter + self.item 
            else:
                raise RuntimeError, 'impossible has happened'
        sh_file.write('%s="%s"\n' % (self.var,new_value_sh))
        sh_file.write("export %s\n" % self.var)
        csh_file.write('if ( $?%s ) then\n' % self.var)
        csh_file.write('    setenv %s "%s"\n' % (self.var, new_value_csh))
        csh_file.write('else\n')
        csh_file.write('    setenv %s "%s"\n' % (self.var, self.item))
        csh_file.write('endif\n')
        sh_file.close()
        csh_file.close()        

    def get_node(self):
        return self.node

class Environment_mod_ctct_file(Environment_mod):
    def __init__(self,filename):
        f = open(filename,"r")
        var = f.readline().rstrip()
        item = f.readline().rstrip().replace('CONTRACTOR_NEWLINE','\n')
        action = int(f.readline())
        delimiter = f.readline().rstrip()
        f.close()
        Environment_mod.__init__(self,var,item,action,delimiter)
        self.current_env_modify()


