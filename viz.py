#!/usr/bin/env python

import os
import signal
import time

try:
    import pydot
    _have_pydot = 1
except:
    _have_pydot = 0

def get_status_color(status):
    if status == "unknown":
        return "gray75"
    elif status == "building":
        return "lawngreen"
    elif status == "done":
        return "dodgerblue"
    else:
        return "crimson"

def update_node(graph,node,status):
    the_node = graph.get_node(node)
    if the_node:
        the_node.set_style("filled")
        the_node.set_color(get_status_color(status))
        the_node.set_fillcolor(get_status_color(status))
        
_current_package = None
def update_graphs(node,status):
    if _package_graph:
        global _current_package
        pair = node.split('/')
        if len(pair)>1:
            package = pair[0]
        else:
            package = node
#        print "setting",package,"and",node,"to",status
        update_node(_package_graph,package,status)
        if package != _current_package:
            _current_package = package
            create_nodes_graph(package)
        update_node(_nodes_graph,node,status)
        display_graphs()

_package_graph = None
_nodes = None
def create_package_graph(nodes,colorize=1):
    global _package_graph
    global _nodes
    _nodes = nodes
    edges = []
    for key in nodes.keys():
        if len(key.split('/')) > 1:
            pkg = key.split('/')[0]
            for dep in nodes[key].dependencies:
                if len(dep.get_name().split('/')) > 1:
                    dep_pkg = dep.get_name().split('/')[0]
                    if dep_pkg != pkg:
                        edges.append((pkg,dep_pkg))
    _package_graph = pydot.graph_from_edges(edges,directed=1)
    if colorize:
        for node in _package_graph.get_node_list():
            update_node(_package_graph,node.get_name(),"unknown")

_nodes_graph = None
def create_nodes_graph(current_package):
    global _nodes_graph
    global _nodes
    nodes = _nodes
    edges = []
    
    for key in nodes.keys():
        if len(key.split('/')) > 1:
            pkg = key.split('/')[0]
            if pkg == current_package:
                for dep in nodes[key].dependencies:
                    if len(dep.get_name().split('/')) > 1:
                        dep_pkg = dep.get_name().split('/')[0]
                        if dep_pkg == pkg:
                            edges.append((key,dep.get_name()))
    _nodes_graph = pydot.graph_from_edges(edges,directed=1)
    for node in _nodes_graph.get_node_list():
        update_node(_nodes_graph,node.get_name(),"unknown")

_package_graph_pid = None
def _display_package_graph():
    global _package_graph_pid
    if _package_graph_pid:
        os.kill(_package_graph_pid,signal.SIGHUP)
        os.waitpid(_package_graph_pid,0)
    _package_graph_pid = os.fork()
    if _package_graph_pid == 0:
        _package_graph.write_png("packages.png")
        os.execvp("display", ["display","packages.png"])

_nodes_graph_pid = None
def _display_nodes_graph():
    global _nodes_graph_pid
    global nodes_graph
    if _nodes_graph:
        if _nodes_graph_pid:
            os.kill(_nodes_graph_pid,signal.SIGHUP)
            os.waitpid(_nodes_graph_pid,0)
        _nodes_graph_pid = os.fork()
        if _nodes_graph_pid == 0:
            _nodes_graph.write_png("nodes.png")
            os.execvp("display", ["display","nodes.png"])

def display_graphs():
    _display_nodes_graph()
    _display_package_graph()

def gee_viz_main(nodes):
    print "gee_viz is temporarily out of service"
    #~ create_package_graph(nodes)
    #~ display_graphs()

def viz_main(graph,filename):
    edges = []
    nodes = graph.get_nodes()
    for key in nodes.keys():
        for dep in nodes[key].dependencies:
            if graph.in_graph(dep):
                edges.append((key,dep.get_name()))
    g = pydot.graph_from_edges(edges,directed=1)
    g.write_ps(filename)
    print "wrote",filename

