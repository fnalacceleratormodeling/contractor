#!/usr/bin/env python

import os,sys,glob,time
import timer
from runtime import global_runtime_options

try:
    from subprocess import Popen,PIPE,STDOUT
except ImportError:
    ### subprocess was not present before Python 2.4. Workaround if needed.
    import popen2
    PIPE=1
    STDOUT=1
    class Popen:
        def __init__(self,command,shell=True,
                stdout=PIPE,stderr=STDOUT):
                if (shell != True) or (stdout != PIPE) or (stderr != STDOUT):
                    raise RuntimeError,"Popen hack only compatible with certain inputs"
                self.popen4 = popen2.Popen4(command)
                self.stdout = self.popen4.fromchild
        
        def poll(self):
            self.returncode = self.popen4.poll()
            if self.returncode == -1:
                return None
            else:
                return self.returncode

start_time = -1.7724539
def create_tag(target):
    f = open("%s" % target[0],'w')
    f.write('%s (%f)\n' % (time.strftime("%Y-%m-%d %H:%M:%S"),time.time()))
    f.close()

# Set Attribute Mode	<ESC>[{attr1};...;{attrn}m
#     * Sets multiple display attribute settings. The following lists
#       standard attributes:

# 0	Reset all attributes
# 1	Bright
# 2	Dim
# 4	Underscore	
# 5	Blink
# 7	Reverse
# 8	Hidden

# 	Foreground Colours
# 30	Black
# 31	Red
# 32	Green
# 33	Yellow
# 34	Blue
# 35	Magenta
# 36	Cyan
# 37	White

# 	Background Colours
# 40	Black
# 41	Red
# 42	Green
# 43	Yellow
# 44	Blue
# 45	Magenta
# 46	Cyan
# 47	White

if sys.stdout.isatty():
    command_pre = "\x1B[01;34m"
    command_post = "\x1B[00m"
    time_pre = "\x1B[02;32m"
    time_post = "\x1B[00m"
    error_msg_pre = "\x1B[00;41;37m"
    error_msg_post = "\x1B[00m"
    error_pre = "\x1B[00;31m"
    error_post = "\x1B[00m"
else:
    command_pre = "*** contractor command: "
    command_post = ""
    time_pre = "--- contractor time: "
    time_post = ""
    error_msg_pre = "XXXXXXXXXXXXXX "
    error_msg_post = ""
    error_pre = ""
    error_post = ""
    
def command_message(description):
    print "%s[%s]%s"%(command_pre,description,command_post)
 
def system_or_die(command_in, log_file=None,command_log_file=None,
    log_stdout=None,raise_exception=False):
    t0 = time.time()
    if log_stdout == None:
        log_stdout = global_runtime_options.verbose_build
    print "%s%s%s"%(command_pre,command_in,command_post)
    if global_runtime_options.dry_run:
        return 0
    else:
        if command_log_file:
            if not os.path.isdir(os.path.dirname(command_log_file)):
                os.makedirs(os.path.dirname(command_log_file))
            f = open(command_log_file,"a")
            f.write("%s\n" % command_in)
            f.close()
        if log_file:
            if not os.path.isdir(os.path.dirname(log_file)):
                os.makedirs(os.path.dirname(log_file))
            log_file_file = open(log_file,"w")
        subproc = Popen(command_in,shell=True,
            stdout=PIPE,stderr=STDOUT)
        while True:
            o = subproc.stdout.readline()
            if o == '' and subproc.poll() != None: 
                break
            if log_stdout:
                sys.stdout.write(o)
            if log_file:
                log_file_file.write(o)
        if log_file:
            log_file_file.close()
            t1 = time.time()
            f = open(log_file + ".time","w")
            f.write("%0.4g seconds\n" % (t1-t0))
            f.close()
        if subproc.returncode!= 0:
            if raise_exception:
                raise RuntimeError, "%s failed" % command_in
            if log_file:
                print "%scontractor failed command output (final 10 lines):%s" % \
                      (error_msg_pre,error_post)
                os.system('tail -n 10 %s' % log_file)
                print "%scontractor failed command output end.%s" % \
                      (error_msg_pre,error_post)
                sys.stdout.write("%slog file is %s%s\n" % \
                                 (error_pre,log_file,error_post))
            timer.show_timer()
            #raise RuntimeError, 'Failed system command'
            sys.exit(1)
        return subproc.returncode

def abs_rel_dir(base_dir, dir):
    if os.path.isabs(dir):
        retval = dir
    else:
        retval = os.path.join(base_dir,dir)
    return retval

def listify(x):
    if type(x) == type([]) or type(x) == type(()):
        return x
    else:
        return [x]

class Changed_files:
    def __init__(self, path):
        self.old = {}
        self.new = {}
        self.new_files = []
        self.modified_files = []
        self.path = path
        os.path.walk(self.path,self.walker,self.old)
        
    def walker(self, arg, dirname, names):
        for name in names:
            fullpath = os.path.join(dirname,name)
            if not os.path.isdir(fullpath):
                try:
                    arg[fullpath] = os.path.getmtime(fullpath)
                except OSError:
                    arg[fullpath] = None

    def end(self):
        os.path.walk(self.path,self.walker,self.new)
        for key in self.new.keys():
            if self.old.has_key(key):
                if self.old[key] != self.new[key]:
                    self.modified_files.append(key)
            else:
                self.new_files.append(key)

    def _dump(self,filename,pathlists):
        if not global_runtime_options.dry_run:
            f = open(filename,"w")
            for pathlist in pathlists:
                for path in pathlist:
                    f.write("%s\n" % path)
            f.close()
        
    def dump_new(self, filename):
        self._dump(filename,[self.new_files])
        
    def dump_modified(self, filename):
        self._dump(filename,[self.modified_files])

    def dump_all(self, filename):
        self._dump(filename,[self.new_files,self.modified_files])

