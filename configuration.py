#!/usr/bin/env python

from nodes import *
from utils import *

import string
import re
import commands
import tempfile
from runtime import global_runtime_options

_options = {}

class Option:
    def __init__(self,root,name,default,type,description,advanced=False):
        self.root = root
        self.name = name
        if callable(default):
            self.function =default
            self.default = ""
        else:
            self.function = None
            self.default = str(default)
        self.type = type
        self.description = description
        self.source = "default"
        self.filename = os.path.join(self.root.get_vars()["config_dir"],
                                     self.name)
        _options[self.name] = self
        self.advanced = advanced

    def _have_value(self):
        return os.path.exists(self.filename)

    def _get(self):
        if self._have_value():
            f = open(self.filename,"r")
            source = f.readline()
            source = source.rstrip()
            value = f.readline()
            f.close()
        else:
            source = "default"
            value = self.default
        return (source,value)

    def _write_value(self,source,value):
        dirname = os.path.dirname(self.filename)
        if not os.path.isdir(dirname):
            os.makedirs(dirname)
        f = open(self.filename, "w")
        f.write("%s\n" % source)
        f.write("%s" % str(value))

    def get(self):
        if not self._have_value():
           self.configure()
        (source,val) = self._get()
        if self.type == bool:
            if (val == "True" or val == "true" or val == "t" or val == "1"):
                retval = True
            elif (val == "False" or val == "false" or val == "f" or val == "0"
                or val == "nil"):
                retval = False
            else:
                raise RuntimeError('Cannot convert "' + val + '" to boolean')
        else:
            retval = self.type(val)
        return retval

    def configure(self, user_opts={}):
        if user_opts.has_key(self.name):
            source = "user"
            value = user_opts[self.name]
        else:
            if self._have_value():
                (source,value) = self._get()
            elif self.function:
                source = "function"
                global_runtime_options.debug_config.write(
                    "debug_config: calling " + str(self.function) +
                        " to get default value for " + self.name
                        + "\n")
                value = str(self.function())
            else:
                value = self.default
                source = "default"
        if self._have_value():
            (old_source,old_value) = self._get()
            if value != old_value:
                self._write_value(source,value)
        else:
            self._write_value(source,value)

    def help(self):
        print self.name,":",self.description,"(default=%s)" % self.default

    def display(self,output,show_source):
        (source,value) = self._get()
        output.write("%s=%s" % (self.name,value))
        if show_source:
            output.write(" (%s)" % source)
        output.write("\n")

    def reset(self):
        if self._have_value():
            os.remove(self.filename)

    def is_advanced(self):
        return self.advanced

def configure_error(message):
    sys.stderr.write("Configuration error:\n%s\n" % message)
    sys.exit(1)

def configure(local_root, args, allow_unknown = False):
    user_opts = {}
    for arg in args:
        pair = string.split(arg,"=")
        if len(pair) < 2:
            configure_error('Improper configure argument "%s".\nArgument should be of the form "option=value".' % arg)
        option_name = pair[0]
        value = string.join(pair[1:],"=")
        if not _options.has_key(option_name):
            if allow_unknown:
                Option(local_root,option_name,value,str,
                  "temporary option")
            else:
                configure_error('Unknown configure option "%s".' % option_name)
        user_opts[option_name] = value
    for key in _options:
        _options[key].configure(user_opts)
    sys.exit(0)

def configure_help():
    for key in _options:
        _options[key].help()
    sys.exit(0)

def configure_show(argv,advanced=False):
    if len(argv) > 0:
        package = argv[0]
    else:
        package = None
    keys = _options.keys()
    keys.sort()
    for key in keys:
        show = True
        if package:
            show = False
            if key.split('/')[0] == package:
                show = True
            elif key.split('_')[0] == package:
                show = True
        if advanced != _options[key].is_advanced():
            show = False
        if show:
            _options[key].display(sys.stdout,show_source=1)
    sys.exit(0)

def configure_dump(args):
    if len(args) != 1 :
        configure_error("dump requires (one) filename")
    filename = args[0]
    f = open(filename,"w")
    for key in _options:
        _options[key].display(f,show_source=0)
    f.close()
    sys.exit(0)

def configure_import(local_root,args):
    if len(args) != 1 :
        configure_error("import requires (one) filename")
    filename = args[0]
    f = open(filename,"r")
    args = []
    for line in f.readlines():
        arg = line.rstrip('\n')
        if len(arg) > 0:
            args.append(arg)
    f.close()
    configure(local_root,args,allow_unknown = True)
    sys.exit(0)

def configure_reset():
    for key in _options:
        _options[key].reset()
    sys.exit(0)

class Platform:
    def __init__(self):
        (status,uname) = commands.getstatusoutput("uname -a")
        if status:
            raise RuntimeError, 'uname -a failed with output %s' % uname
        self.uname_tuple = uname.split()
        self.linux = self.uname_tuple.count('Linux')
        self.macosx = self.uname_tuple.count('Darwin')
        self.i386 = self.uname_tuple.count('i386')
        self.i686 = self.uname_tuple.count('i686')
        self.athlon = self.uname_tuple.count('athlon')
        self.x86_64 = self.uname_tuple.count('x86_64')

    def get_uname(self):
        return self.uname_tuple.join()

    def get_tuple(self):
        return self.uname_tuple

    def is_linux(self):
        return self.linux

    def is_macosx(self):
        return self.macosx

    def is_i386(self):
        return self.i386

    def is_i686(self):
        return self.i686

    def is_athlon(self):
        return self.athlon

    def is_x86_64(self):
        return self.x86_64

platform = Platform()

def version_acceptable(the_str,min_version_list):
    global_runtime_options.debug_config.write(
        "debug_config: working on found version str '%s'" % the_str
        + "\n")
    version_num_match = re.search('[0-9.]+', the_str)
    if version_num_match:
        version_num_str = version_num_match.group(0)
        garbled_str = False
        try:
            version_num_list = map(int,version_num_str.split('.'))
        except:
            garbled_str = True
            found_good_version = False
        if not garbled_str:
            global_runtime_options.debug_config.write(
                "debug_config: comparing found version " + str(version_num_list) +
                " with minimum version " + str(min_version_list)
                + "\n")
            for i in range(0,len(min_version_list)):
                if len(version_num_list) < i+1:
                    version_num_list.append(0)
                if version_num_list[i] > min_version_list[i]:
                    found_good_version = True
                    break
                elif version_num_list[i] < min_version_list[i]:
                    found_good_version = False
                    break
                elif version_num_list[i] == min_version_list[i]:
                    found_good_version = True
    else:
        found_good_version = False
    global_runtime_options.debug_config.write(
        "debug_config: found_good_version = " + str(found_good_version)
        + "\n")
    return found_good_version

def need_internal_executable(executable):
    cmd = 'type %s' % executable
    global_runtime_options.debug_config.write(
        "debug_config: running command " + cmd + "\n")
    (status,output) = commands.getstatusoutput(cmd)
    global_runtime_options.debug_config.write(
        "debug_config: execution completed with status " + str(status) +
            " and output:\n" +
        output
        + "\n")
    if status:
        retval = 1
    else:
        retval = 0
    return retval

def need_internal_version_executable(command,min_version_list,regexp=None):
    global_runtime_options.debug_config.write(
        "debug_config: running command " + command
        + "\n")
    (status,output) = commands.getstatusoutput(command)
    global_runtime_options.debug_config.write(
        "debug_config: execution completed with status " + str(status) +
            " and output:\n"
        + output
        + "\n")
    if status:
        found_good_version = False
    else:
        if regexp:
            match = re.search(regexp,output)
            if match:
                output = match.group(0)
            else:
                output = ''
        found_good_version = version_acceptable(output,min_version_list)
    if found_good_version:
        retval = 0
    else:
        retval = 1
    return retval

def need_internal_version_library(headers,body="",
        include_flags="",link_flags="", compiler="g++"):
    global_runtime_options.debug_config.write(
        "debug_config: test compiling C++ program:\n"
        + "----------------begin----------------"
        + "\n")
    olddir = os.getcwd()
    tmpdir = tempfile.mkdtemp()
    os.chdir(tmpdir)
    headers_list = listify(headers)
    f = open("tmp.cc","w")
    for header in headers_list:
        f.write('#include "%s"\n' % header)
    f.write('int main()\n{\n%s\n return 0;\n}\n' % body)
    f.close()
    f = open("tmp.cc","r")
    map(global_runtime_options.debug_config.write,f.readlines())
    f.close()
    global_runtime_options.debug_config.write(
        "-----------------end-----------------"
        + "\n")
    command = compiler + ' %s tmp.cc -o a.out %s' % (include_flags,link_flags)
    global_runtime_options.debug_config.write(
        "debug_config: " + command
        + "\n")
    (status,output) = commands.getstatusoutput(command)
    global_runtime_options.debug_config.write(
        "debug_config: compilation completed with status " + str(status)
        + " and output:\n"
        + output
        + "\n")
    retval = 0
    if status:
        retval = 1
    else:
        global_runtime_options.debug_config.write(
            "debug_config: executing ./a.out:"
            + "\n")
        (status,output) = commands.getstatusoutput('./a.out')
        global_runtime_options.debug_config.write(
            "debug_config: execution completed with status " + str(status)
                + " and output: "
                + output
                + "\n")
        if status:
            retval = 1
    os.chdir(olddir)
    (status,output) = commands.getstatusoutput('/bin/rm -r %s' % tmpdir)
    return retval

def need_internal_version_python_module(module,min_version_list):
    cmd = 'python -c "import %s; print %s.__version__"' % (module,module)
    global_runtime_options.debug_config.write(
        "debug_config: running command " + cmd
        + "\n")
    (status,output) = commands.getstatusoutput(cmd)
    global_runtime_options.debug_config.write(
        "debug_config: command completed with status " + str(status)
        + " and output:\n"
        + output
        + "\n")
    if status:
        found_good_version = False
    else:
        found_good_version = version_acceptable(output,min_version_list)
    if found_good_version:
        retval = 0
    else:
        retval = 1
    return retval
