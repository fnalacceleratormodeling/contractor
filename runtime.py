#!/usr/bin/env python
import os
import sys

class Runtime_options:
    def __init__(self):
        self.verbose_build = False
        self.dry_run = False
        config_log = 'config_log'
        if os.environ.has_key('DEBUG_CONFIG'):
            if (os.environ['DEBUG_CONFIG'] == "0") or \
                (os.environ['DEBUG_CONFIG'].lower() == "false"):
                self.debug_config = open(config_log, 'w')
            else:
                self.debug_config = sys.stdout
        else:
            self.debug_config = open(config_log, 'w')

global_runtime_options = Runtime_options()
