#!/usr/bin/env python
import os,sys,glob,time

from utils import *
import utils
from configuration import *
import timer
from viz import *
from root import local_root
import string
from runtime import global_runtime_options
from bundle import list_bundles, create_bundle

def usage(retval=0):
    print "usage: contractor.py [options] [targets]"
    print "options:"
    print "  --help                    : this message"
    print "  --verbose                 : show output of build commands"
    print "  --configure [args]        : configure build options"
    print "  --configure-help          : list all build options"
    print "  --configure-show [pkg]    : show values of build options"
    print "  --configure-show-advanced [pkg]: show values of advanced build options"
    print "  --configure-dump <file>   : dump build options to file"
    print "  --configure-import <file> : import build options from file"
    print "  --configure-reset         : reset configuration to default"
    print "  --list-targets            : list designated targets"
    print "  --list-nodes              : list all defined nodes"
    print "  --default                 : list default targets"
    print "  --deps <target>           : show dependencies for <target>"
    print "  --viz                     : create packages graph (requires pydot)"
    print "  --gee-viz                 : display live dependency graphs"
    print "  --dry-run                 : show, but do not execute, build commands"
    print "  --list-bundles            : list available bundle labels"
    print "  --bundle <label>          : create bundle <label"
    sys.exit(retval)

def list_targets(graph,prefix=None):
    targets = graph.get_target_names()
    if len(targets) > 0 :
        for target in targets:
            if prefix:
                combined_name = prefix + '/' + target
            else:
                combined_name = target
            print combined_name,
            if graph.get_default_name() == target:
                print "(default)"
            else:
                print
            list_targets(graph.get_node(target).get_graph(),
                '  ' + combined_name)
    else:
        if prefix == None:
            print "No targets defined."

def list_nodes(graph,prefix=None):
    nodes = graph.get_node_names()
    if len(nodes) > 0:
        for node in nodes:
            if prefix:
                combined_name = prefix + '/' + node
            else:
                combined_name = node
            print combined_name
            list_nodes(graph.get_node(node).get_graph(),
                '  ' + combined_name)
    else:
        if prefix == None:
            print "No nodes defined."

def list_default(graph):
    print graph.get_default_name()

def list_deps(graph,node_name,retval=0):
    if not graph.get_nodes().has_key(node_name):
        print "Unknown node '%s'. Defined nodes:" % node_name
        list_nodes(graph)
        sys.exit(1)
    graph.get_node(node_name).list_dependencies()
    sys.exit(retval)
    
def build_node(graph,node):
    try:
        parts = node.split('/')
        parent = parts[0]
        children = parts[1:]
        if not graph.has_node(parent):
            print "Unknown node '%s'. Defined nodes:" % node
            list_nodes(graph)
            sys.exit(1)
        retval = -1
        if len(children) == 0:
            retval = graph.get_node(parent).build()
        else:
            retval = graph.get_node(parent).build(string.join(children,'/'))
        if not retval:
            print "nothing to be done for",node
    except RuntimeError, e:
                sys.stderr.write("Contractor error: %s\n" % e)
                sys.exit(1)

def get_nodes():
    return _nodes

try:
    import pydot

    def viz():
        viz_main(local_root.get_package_graph(),'packages.ps')
        sys.exit(0)
        
    def gee_viz():
        gee_viz_main(_nodes)
        
except:
    def viz():
        print "pydot must be installed in order to use the viz option"
        sys.exit(1)            

    def gee_viz():
        print "pydot must be installed in order to use the gee-viz option"
        sys.exit(1)            
        
def main(argv):
    argv.reverse()
    if len(argv) > 0:
        argv.pop()
    found_target = 0
    while len(argv) > 0:
        arg = argv.pop()
        if arg == "--help":
            usage(0)
        if arg == "--verbose":
            global_runtime_options.verbose_build = True
        elif arg == "--configure":
            configure(local_root,argv)
        elif arg == "--configure-help":
            configure_help()
        elif arg == "--configure-show":
            configure_show(argv,advanced=False)
        elif arg == "--configure-show-advanced":
            configure_show(argv,advanced=True)
        elif arg == "--configure-dump":
            configure_dump(argv)
        elif arg == "--configure-import":
            configure_import(local_root,argv)
        elif arg == "--configure-reset":
            configure_reset()
        elif arg == "--list-targets":
            list_targets(local_root.get_graph())
            sys.exit(0)
        elif arg == "--list-nodes":
            list_nodes(local_root.get_graph())
            sys.exit(0)
        elif arg == "--default":
            list_default(local_root.get_graph())
            sys.exit(0)
        elif arg == "--deps":
            if len(argv) > 0:
                dep_arg = argv.pop()
            else:
                usage(1)
            list_deps(local_root.get_graph(),dep_arg)
        elif arg == "--viz":
            viz()
        elif arg == "--gee-viz":
            gee_viz()
        elif arg == "--dry-run":
            global_runtime_options.dry_run = True
        elif arg == "--list-bundles":
            list_bundles()
            sys.exit(0)
        elif arg == "--bundle":
            if len(argv) > 0:
                label = argv.pop()
            else:
                usage(1)
            create_bundle(label)
            sys.exit(0)
        elif arg[0] == "-":
            print 'unknown option "%s"' % arg
            usage(1)
        else:
            timer.reset_timer()
            if global_runtime_options.dry_run:
                setup_dry_run_db()
            build_node(local_root.get_graph(),arg)
            if global_runtime_options.dry_run:
                teardown_dry_run_db()
            timer.show_timer()
            found_target = 1
    if not found_target:
        timer.reset_timer()
        if global_runtime_options.dry_run:
            setup_dry_run_db()
        build_node(local_root.get_graph(),local_root.get_graph().get_default_name())
        if global_runtime_options.dry_run:
            teardown_dry_run_db()
        timer.show_timer()

def setup_dry_run_db():
    db_dir = local_root.get_var("db_dir")
    dry_run_db_dir = db_dir +"-dryrun"
    if os.path.exists(dry_run_db_dir):
        os.system("/bin/rm -r %s" % dry_run_db_dir)
    if os.path.exists(db_dir):
        os.system("/bin/cp -r %s %s" % (db_dir,dry_run_db_dir))

def teardown_dry_run_db():
    db_dir = local_root.get_var("db_dir")
    dry_run_db_dir = db_dir +"-dryrun"
    if os.path.exists(db_dir):
       os.system("/bin/rm -r %s" % db_dir)
    if os.path.exists(dry_run_db_dir):
       os.system("/bin/mv %s %s" % (dry_run_db_dir , db_dir))
     
