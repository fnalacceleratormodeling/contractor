#!/usr/bin/env python
import os,sys,glob,time

from utils import *
from nodes import *
from graph import Graph
from environment import *
import pickle
import glob
import platform

class Root(Node):
    def __init__(self, base=None, db="db", build="build",
                 install="install"):
        Node.__init__(self,'root')
        self.vars = {}
        if base:
            self.vars['base_dir'] = os.path.abspath(base)
        else:
            self.vars['base_dir'] = os.path.abspath(os.getcwd())
        self.vars['db_dir'] = abs_rel_dir(self.vars['base_dir'],db)
        self.vars['archive_dir'] = \
                                 abs_rel_dir(self.vars['base_dir'],"archives")
        self.vars['urlcache_dir'] = \
                                  abs_rel_dir(self.vars['base_dir'],"urlcache")
        self.vars['config_dir'] = \
                                  abs_rel_dir(self.vars['base_dir'],"config")
        self.vars['build_dir'] = abs_rel_dir(self.vars['base_dir'],build)
        self.vars['install_dir'] = abs_rel_dir(self.vars['base_dir'],install)
        self.vars['setup_dir'] = abs_rel_dir(self.vars['base_dir'],"setup.d")

        self.vars['tag_dir'] = os.path.join(self.vars['db_dir'],"tags")
        self.vars['log_dir'] = os.path.join(self.vars['db_dir'],"logs")

        self.extra_dist_list = []
        self._init_env()
        self.package_graph = Graph()
        self.graph.add_node(Command('dist',self._dist),target=1)
        self.graph.add_node(Command('clean',self._clean),target=1)
        self.graph.add_node(Command('destroy',self._destroy),target=1)
        root_setup = Dummy_node('root_setup')
        self.graph.add_node(root_setup)
        self.graph.add_node(File('create_setup_sh','setup.sh', 
            self._create_setup_sh))
        self.graph.add_node(File('create_setup_csh','setup.csh', 
            self._create_setup_csh))
        self.graph.add_node(Directories('root_dirs',[self.vars['base_dir'],
                                             self.vars['db_dir'],
                                             self.vars['archive_dir'],
                                             self.vars['urlcache_dir'],
                                             self.vars['build_dir'],
                                             self.vars['install_dir'],
                                             self.vars['tag_dir'],
                                             self.vars['log_dir'],
                                             self.vars['config_dir'],
                                             self.vars['setup_dir']]))
        self.env_path = Environment_mod_node(self,'root','PATH',
            os.path.join(self.vars['install_dir'],'bin'))
        self.graph.add_node(self.env_path.get_node())
        self.dynamic_lib_var = 'LD_LIBRARY_PATH'
        if platform.system() == 'Darwin':
            self.dynamic_lib_var = 'DYLD_LIBRARY_PATH'
        self.env_ld_lib_path = Environment_mod_node(self,'root',
            self.dynamic_lib_var, os.path.join(self.vars['install_dir'],'lib'))
        self.graph.add_node(self.env_ld_lib_path.get_node())

        root_setup.depends(self.graph.get_node('create_setup_sh'))
        root_setup.depends(self.graph.get_node('create_setup_csh'))
        root_setup.depends(self.graph.get_node('root_dirs'))
        self.env_path.get_node().depends(self.graph.get_node('root_dirs'))
        self.env_ld_lib_path.get_node().depends(self.graph.get_node('root_dirs'))
        root_setup.depends(self.env_path.get_node())
        root_setup.depends(self.env_ld_lib_path.get_node())

        self.tarball_dir = None
        self.other_roots = []
        self.other_package_vars = {}
        #~ self._add_other_roots()

    def add_extra_envs(self):
        global extra_envs
        if extra_envs.get() and (extra_envs.get() != "None"):
            quoted_comma = '\,'
            comma_subst = '__contractor_comma__'
            extra_env_raw_list = string.replace(extra_envs.get(),
                                              quoted_comma, comma_subst).split(',')
            self.extra_envs = []
            for extra_env_raw in extra_env_raw_list:
                extra_env_str = string.replace(extra_env_raw, comma_subst, ',')
                (name, value) = extra_env_str.split('=')
                extra_env = Environment_mod_node(self, 'root', name, value,
                                                 action=ENV_SET)
                self.extra_envs.append(extra_env)
                self.graph.add_node(extra_env.get_node())
                self.graph.get_node('root_setup').depends(extra_env.get_node())

    def add_extra_paths(self):
        global extra_paths
        if extra_paths.get() and (extra_paths.get() != "None"):
            quoted_comma = '\,'
            comma_subst = '__contractor_comma__'
            extra_path_raw_list = string.replace(extra_paths.get(),
                                              quoted_comma, comma_subst).split(',')
            self.extra_paths = []
            index = 0
            extra_path_raw_list.reverse()
            for extra_path_raw in extra_path_raw_list:
                extra_path_str = string.replace(extra_path_raw, comma_subst, ',')
                extra_path = Environment_mod_node(self, 'root', 'PATH',
                                                  extra_path_str,
                                                  action=ENV_PREPEND,
                                                  index=index)
                self.extra_paths.append(extra_path)
                self.graph.add_node(extra_path.get_node())
                self.graph.get_node('root_setup').depends(extra_path.get_node())
                index += 1

    def add_extra_libpaths(self):
        global extra_libpaths
        if extra_libpaths.get() and (extra_libpaths.get() != "None"):
            quoted_comma = '\,'
            comma_subst = '__contractor_comma__'
            extra_libpath_raw_list = string.replace(extra_libpaths.get(),
                                                    quoted_comma,
                                                    comma_subst).split(',')
            self.extra_libpaths = []
            index = 0
            extra_libpath_raw_list.reverse()
            for extra_libpath_raw in extra_libpath_raw_list:
                extra_libpath_str = string.replace(extra_libpath_raw, comma_subst, ',')
                extra_libpath = Environment_mod_node(self, 'root',
                                                     self.dynamic_lib_var,
                                                     extra_libpath_str,
                                                     action=ENV_PREPEND,
                                                     index=index)
                self.extra_libpaths.append(extra_libpath)
                self.graph.add_node(extra_libpath.get_node())
                self.graph.get_node('root_setup').depends(extra_libpath.get_node())
                index += 1

    def add_other_roots(self):
        global other_roots
        if other_roots.get() != "":
            path_list = other_roots.get().split(',')
            other_roots_dir = os.path.join(local_root.get_var('db_dir'),'other_roots')
            if os.path.exists(other_roots_dir):
                os.removedirs(other_roots_dir)
            os.makedirs(other_roots_dir)
            sys.path.insert(0,other_roots_dir)
            index=0
            for path in path_list:
                if os.path.exists(os.path.join(path,'contract.py')):
                    root = Root(path)
                    self.other_roots.append(root)
                    for pkg_path in glob.glob(os.path.join(root.get_var('tag_dir'),'*')):
                        name = os.path.basename(pkg_path)
                        pickle_filename = os.path.join(pkg_path,name+'_vars.pickle')
                        if os.path.exists(pickle_filename):
                            f = open(pickle_filename,'r')
                            self.other_package_vars[name] = pickle.load(f)
                            f.close()
                else:
                    print "invalid other root"
                    print "contract.py not found in other root path '%s'" % path
    
    def has_other_package(self,package):
        return self.other_package_vars.has_key(package)

    def get_other_package_vars(self,package):
        return self.other_package_vars[package]

    def add_package(self,package):
        package.depends(self.graph.get_node('root_setup'))
        self.graph.add_node(package,target=1)
        self.package_graph.add_node(package)
    
    def get_package_graph(self):
        return self.package_graph
        
    def _init_env(self):
        for ctct_file in glob.glob(os.path.join(self.vars['setup_dir'],'*.ctct')):
            Environment_mod_ctct_file(ctct_file)

    def _create_setup_sh(self,filename):
        f = open(filename,"w")
        for root in self.other_roots:
            f.write('. "%s"\n' %\
                os.path.join(root.get_var('base_dir'),'setup.sh'))
        f.write('for setupsh in %s/*.sh\n' % self.vars['setup_dir'])
        f.write('do\n')
        f.write('    . $setupsh\n')
        f.write('done\n')

    def _create_setup_csh(self,filename):
        f = open(filename,"w")
        for root in self.other_roots:
            f.write('source "%s"\n' %\
                os.path.join(root.get_var('base_dir'),'setup.sh'))
        f.write('foreach setupcsh ( %s/*.csh )\n' % self.vars['setup_dir'])
        f.write('    source $setupcsh\n')
        f.write('end\n')

    def get_vars(self):
        return self.vars

    def get_var(self,name):
        return self.vars[name]

    def get_name(self):
        return 'root'

    def get_dist(self):
        return self.graph.get_node('dist')

    def dirs_node(self):
        return self.graph.get_node('root_dirs')

    def extra_dist(self,name):
        self.extra_dist_list.append(name)

    def set_tarball_dir(self,name):
        self.tarball_dir = name

    def _dist(self):
        os.chdir(self.vars['base_dir'])
        os.chdir("..")
        orig_dir_name = os.path.basename(self.vars['base_dir'])
        tmp_move = 0
        if self.tarball_dir:
            dir_name = self.tarball_dir
            if os.path.exists(dir_name):
                tmp_move = 1
                os.rename(dir_name, dir_name+'_contractor_tmp')
            os.rename(orig_dir_name,dir_name)
        else:
            dir_name = orig_dir_name
        tarfile_name = os.path.join(dir_name,"%s.tar.gz" % dir_name)
        content_args = os.path.join(dir_name,"contract.py")
        svn_dir = os.path.join(dir_name,".svn")
        if os.path.exists(svn_dir):
            content_args += " " + svn_dir 
        content_args += " " + os.path.join(dir_name,"contractor")
        content_args += " " + os.path.join(dir_name,"archives")
        content_args += " " + os.path.join(dir_name,"packages")
        for extra in self.extra_dist_list:
            content_args += " " + os.path.join(dir_name,extra)
        command = "tar '--exclude=*.pyc' --exclude='*~' -zcf " + tarfile_name + " " + \
                  content_args
        retval = os.system(command)
        if self.tarball_dir:
            if tmp_move:
                os.rename(dir_name+'contractor_tmp',dir_name)
            os.rename(dir_name,orig_dir_name)
        if not retval:
            print "wrote", os.path.basename(tarfile_name)
        else:
            sys.stderr.write("tarball creation failed!\n")
            sys.exit(1)
        os.chdir(self.vars['base_dir'])
    
    def _clean(self):
        os.system("/bin/rm -rf %s %s %s" % \
            (self.vars['db_dir'], self.vars['install_dir'],
            self.vars['setup_dir']))
        for dir in glob.glob(os.path.join(self.vars['build_dir'],'*')):
            precious = False
            for name in self.package_graph.get_node_names():
                package = self.package_graph.get_node(name)
                if (package.get_var('src_dir') == dir) and \
                        package.is_precious():
                        print "%s is precious. Leaving\n    %s\nuntouched." % \
                            (name,dir)
                        precious = True
            if not precious:
                os.system("/bin/rm -rf %s" % dir)

    def _destroy(self):
        os.system("/bin/rm -rf %s %s %s %s" % \
            (self.vars['db_dir'], self.vars['install_dir'],
            self.vars['setup_dir'], self.vars['build_dir']))

    def set_default_target(self,target):
        self.get_graph().make_default(target)

local_root = Root()
other_roots = Option(local_root,"other_roots","",str,
                            "Comma-separated list of other roots")
local_root.add_other_roots()
extra_envs = Option(local_root, "extra_envs", None, str,
                    "Comma-separated list of extra environment variables, e.g., extra_envs=FOO=foo1,BAR=bar2,BAZ=qux\\,corge")
local_root.add_extra_envs()
extra_paths = Option(local_root, "extra_paths", None, str,
                    "Comma-separated list of extra paths to be prepended to PATH")
local_root.add_extra_paths()
extra_libpaths = Option(local_root, "extra_libpaths", None, str,
                    "Comma-separated list of extra dynamic library paths to be prepended to XXX_LIBRARY_PATH")
local_root.add_extra_libpaths()
default_parallel = Option(local_root, "default_parallel", 2, int,
                          "Default number of processes for parallel builds")
#~ tarball_name_option = Option(local_root,"tarball_name",
        #~ os.path.basename(local_root.get_var('base_dir')),str,
        #~ "base name for dist tarball")
#~ local_root.set_tarball_dir(tarball_name_option.get())
