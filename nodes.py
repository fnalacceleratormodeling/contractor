#!/usr/bin/env python
import os,sys,glob,time

from utils import *
from configuration import *
import timer
from viz import *
from graph import *

class Node:
    def __init__(self, name):
        self.name = name
        self.dependencies = []
        self.graph = Graph()

    def get_name(self):
        return self.name

    def get_graph(self):
        return self.graph
    
    def is_modified(self, parent):
        return 1

    def modification_time(self):
        return None

    def reset(self):
        pass

    def exists(self):
        return 0

    def recursive_reset(self):
        for dependency in self.dependencies:
            dependency.recursive_reset()

    def depends(self,dependency):
        self.dependencies.append(dependency)
        self._check_circularity()

    def list_dependencies(self, prefix=""):
        if prefix == "":
            print "n.b. This dependency listing is confusing and should be fixed"
        print prefix,self.name
        for dependency in self.dependencies:
            dependency.list_dependencies(prefix+"    ")
        
    def build(self,subtarget=None):
        dependency_modified = 0
        built = 0
        # build external dependencies
        for dependency in self.dependencies:
            if not self.graph.in_graph(dependency):
                dependency.build()
        first_child = None
        children = None
        if subtarget:
            parts = subtarget.split('/')
            first_child = parts[0]
            if len(parts[1:]) > 0:
                children = string.join(parts[1:],'/')
        if first_child:
                return self.graph.get_node(first_child).build(children)
        # build internal dependencies
        for dependency in self.dependencies:
            if self.graph.in_graph(dependency):
                dependency.build()
        for dependency in self.dependencies:
            if dependency.is_modified(self):
                dependency_modified = 1
        if dependency_modified or (not self.exists()):
            update_graphs(self.name,"building")
            if self.graph.non_empty():
                self.graph.get_default().build()
            self.build_method()
            built = 1
        update_graphs(self.name,"done")
        return built

    def build_method(self):
        print "building node '%s'" % self.name

    def _check_circularity(self, parents=[]):
        if parents.count(self)>0:
            raise RuntimeError, 'Circular dependency in %s' % self.name 
        new_parents = list(parents)
        new_parents.append(self)
        for dependency in self.dependencies:
            dependency._check_circularity(new_parents)

    def is_newer(self, other_node):
        my_time = self.modification_time()
        newer = 0
        if other_node == None:
            newer = 0
        else:
            other_time = other_node.modification_time()
            if my_time == None or other_time == None:
                newer = 1
            else:
                newer =  (my_time > other_time)
        return newer
    
class Dummy_node(Node):
    def build_method(self):
        pass
    def is_modified(self, parent):
        return 0
    
class Command(Node):
    def __init__(self, name, command):
        self.command = command
        Node.__init__(self, name)

    def build_method(self):
        self.command()

class Never_modified_command(Command):
    def __init__(self, name, command):
        Command.__init__(self, name, command)

    def is_modified(self,parent):
        return 0
        
class Command_string(Command):
    def build_method(self):
        os.system(self.command)

class File(Node):
    def __init__(self, name, filename=None, create_command=None):
        if filename:
            self.filename = filename
        else:
            self.filename = name
        self.create_command = create_command
        Node.__init__(self,name)
        
    def modification_time(self):
        if os.path.exists(self.filename):
            return os.path.getmtime(self.filename)
        else:
            return None

    def is_modified(self, parent):
        if os.path.exists(self.filename):
            return self.is_newer(parent)
        else:
            return 1

    def exists(self):
        return os.path.exists(self.filename)

    def build_method(self):
        if self.create_command:
            self.create_command(self.filename)
        else:
            raise RuntimeError, 'No such file "%s"' % self.filename

class Files(Node):
    def __init__(self, name, filenames, create_command=None):
        self.filenames = filenames
        self.create_command = create_command
        Node.__init__(self,name)
        
    def modification_time(self):
        time = None
        for filename in self.filenames:
            if os.path.exists(filename):
                file_time = os.path.getmtime(filename)
                if file_time > time:
                    time = file_time
        return time
        
    def is_modified(self, parent):
        for filename in self.filenames:
            if not os.path.exists(filename):
                return 1
        return self.is_newer(parent)
        
    def exists(self):
        exists = 1
        for filename in self.filenames:
            if not os.path.exists(filename):
                exists = 0
        return exists

    def build_method(self):
        if self.create_command:
            self.create_command()
        else:
            raise RuntimeError, 'No such file "%s"' % self.filename

class Directories(Node):
    def __init__(self, name, dirnames=None):
        if dirnames:
            self.dirnames = listify(dirnames)
        else:
            self.dirnames = listify(name)
        Node.__init__(self,name)
        
    def modification_time(self):
        return None

    def is_modified(self, parent):
        return 0

    def exists(self):
        retval = 1
        for dirname in self.dirnames:
            if not os.path.isdir(dirname):
                retval = 0
        return retval

    def build_method(self):
        for dirname in self.dirnames:
            if not os.path.isdir(dirname):
                os.makedirs(dirname)

class Tagged_node(File):
    def build_method(self):
        if not os.path.isdir(os.path.dirname(self.filename)):
            os.makedirs(os.path.dirname(self.filename))
        f = open(self.filename,"w")
        f.write("this is my tag!\n")
        f.close()

class Tagged_command(Tagged_node):
    def __init__(self, name, command, filename=None):
        self.command = command
        File.__init__(self, name, filename)

    def build_method(self):
        self.command()
        Tagged_node.build_method(self)
