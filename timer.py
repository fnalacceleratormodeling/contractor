#!/usr/bin/env python

import time

_start_time = 0.0

def reset_timer():
    global _start_time
    _start_time = time.time()

def show_timer():
    global _start_time
    print "total time:",
    seconds = time.time() - _start_time
    hours = int(seconds/3600)
    if hours > 0:
        print hours,"hours,",
        seconds = seconds - 3600*hours
    minutes = int(seconds/60)
    if minutes > 0:
        print minutes,"minutes,",
        seconds = seconds - 60*minutes
    print "%0.3g seconds" % seconds
