#!/usr/bin/env python
import os,sys,glob,time

from utils import *
from nodes import *
from graph import Graph
from configuration import Option
import pickle

class External_package(Dummy_node):
    def __init__(self, name, prefix=None):
        self.name = name
        self.prefix = prefix
        self.vars = {}
        Dummy_node.__init__(self,name)

    def first_tag(self):
        return self.node

    def last_tag(self):
        return self.node

    def get_name(self):
        return self.name

    def get_var(self,name):
        return self.vars[name]

    def get_vars(self):
        return self.vars

    def new_var(self,name,value):
        self.vars[name] = value % self.vars
    
    def is_external(self):
        return True

def unique_name(name,names):
    count = 0
    while names.count(name) > 0:
        count += 1
        last = len(name)-1
        if name[last].isdigit():
            index = int(name[last])
            if count > 18:
                raise RuntimeError, 'unique_name: too many iterations'
            name = name[0:last] + str(index+1)
        else:
            name += '2'
    names.append(name)
    return name

class Package(Tagged_node):
    def __init__(self, root, name, stages, depends=[], non_depends=[],
                 src_dir=None, build_dir=None, precious=0):
        self.name = name
        self.precious_option = Option(root,
                name+"/precious",
                precious,int,"never delete source directory",advanced=True)
        self.local = 1
        if root.has_other_package(self.name):
            local_option = Option(root,"local_"+self.name,self.is_precious(),int,
                            "Build local version of " + self.name)
            if not local_option.get():
                self.local = 0
        if not self.local:
            self.vars = root.get_other_package_vars(self.name)
            Tagged_node.__init__(self,name,
                os.path.join(self.vars['tag_dir'],name))
            return
        self.root = root
        self._setup_vars(src_dir,build_dir)
        Tagged_node.__init__(self,name,
            os.path.join(self.vars['tag_dir'],name))
        self.graph.add_node(Command("clean", self.clean),
                target=1)
        self.graph.add_node(Command("destroy", self.destroy),
                target=1)
        self.graph.add_node(Never_modified_command("add_dep_vars",
            self.add_dep_vars))
        last_node = self.graph.get_node("add_dep_vars")
        stage_names = []
        for stage in stages:
            substages = [stage]
            if stage.aux_stage():
                substages.append(stage.aux_stage())
            for substage in substages:
                substage.set_package(self)
                stage_name = unique_name(substage.get_name(),stage_names)
                substage.set_name(stage_name)
                tag_file = self._tag_file(stage_name)
                self.graph.add_node(Tagged_command(\
                    stage_name,substage.build_method,tag_file))
                if last_node:
                    self.graph.get_node(stage_name).depends(last_node)
            last_node = self.graph.get_node(stage.get_name())
        self.depends(last_node)
        for dependency in depends:
            self.depends(dependency)
            if len(stages) > 0:
                self.graph.get_node(stages[0].get_name()).depends(dependency)
        self.all_depends = depends + non_depends
        self.did_add_dep_vars = 0
        self.root.add_package(self)
       
    def _setup_vars(self, src_dir, build_dir):
        self.vars = {}
        self.add_vars(self.root)
        if src_dir:
            self.vars['src_dir'] = abs_rel_dir(self.vars['root.build_dir'],
                                                 src_dir)
        else:
            self.vars['src_dir'] = os.path.join(self.vars['root.build_dir'],
                                                self.name)
        if build_dir:
            self.vars['build_dir'] = abs_rel_dir(self.vars['root.build_dir'],
                                                 build_dir)
        else:
            self.vars['build_dir'] = self.vars['src_dir']
        self.vars['log_dir'] = os.path.join(self.vars['root.log_dir'],
                                            self.name)
        self.vars['tag_dir'] = os.path.join(self.vars['root.tag_dir'],
                                            self.name)
    
    def add_dep_vars(self):
        # jfa: this is ugly. add_dep_vars gets re-called every time the package
        # is used as a dependency. did_add_dep_vars is a workaround.
        if not self.did_add_dep_vars:
            for dependency in self.all_depends:
                self.add_vars(dependency)
            self.did_add_dep_vars = 1
    
    def _tag_name(self, stage_name):
        return "%s/%s" % (self.name,stage_name)
    
    def _tag_file(self, stage_name):
        return os.path.join(self.vars['tag_dir'],stage_name)
    
    def get_name(self):
        return self.name
    
    def get_var(self,name):
        return self.vars[name]

    def get_vars(self):
        return self.vars

    def new_var(self,name,value):
        self.vars[name] = value % self.vars

    def add_vars(self, package):
        prefix = package.get_name() + "."
        for key in package.get_vars():
            self.vars[prefix+key] = package.get_var(key)
            
    def is_precious(self):
        return self.precious_option.get()

    def clean_or_destroy(self, destroy=False):
        change_logs = glob.glob(os.path.join(self.vars['log_dir'],
            '*.changed_files'))
        for change_log in change_logs:
            f = open(change_log,"r")
            for line in f.readlines():
                filename = line.rstrip()
                ### sanity check
                if filename.find(self.vars['root.install_dir']) != 0:
                    print "warning: not deleting file",filename
                else:
                    os.system("/bin/rm -f %s" % filename)
        os.system("/bin/rm -rf %(tag_dir)s %(log_dir)s"%\
                  self.vars)
        if self.vars['build_dir'] != self.vars['src_dir']:
            os.system("/bin/rm -rf %(build_dir)s" % self.vars)
        if self.is_precious() and os.path.exists(self.vars['src_dir']) \
            and (not destroy):
            print "%s is precious. Leaving\n    %s\nuntouched." % \
                (self.name,self.vars['src_dir'])
            return
        else:
            os.system("/bin/rm -rf %(src_dir)s" % self.vars)
    
    def clean(self):
        self.clean_or_destroy(False)

    def destroy(self):
        self.clean_or_destroy(True)

    def build_method(self):
        pickle_filename = os.path.join(self.vars['tag_dir'],self.name+'_vars.pickle')
        f = open(pickle_filename,'w')
        pickle.dump(self.vars,f)
        f.close()
        Tagged_node.build_method(self)

    def is_external(self):
        return False
