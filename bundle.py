#!/usr/bin/env python

import tarfile
import glob
import sys
import os
import re

class Package_bundle:
    def __init__(self, package, exclude_patterns = []):
        self.package = package
        self.exclude_patterns = exclude_patterns
    
    def get_install_dir(self):
        return self.package.get_var('root.install_dir')
    
    def get_files(self):
        pattern = os.path.join(self.package.get_var('log_dir'), 
                               '*.changed_files')
        file_list_files = glob.glob(pattern)
        if len(file_list_files) != 1:
            sys.stderr.write('Package ' + self.package.get_name())
            sys.stderr.write(': expected a single *.changed_files entry,\n')
            sys.stderr.write('found ' + str(file_list_files) + '\n')
            sys.exit(1)
        f = open(file_list_files[0], 'r')
        files = []
        install_dir = self.get_install_dir()
        for line in f.readlines():
            full_path = line.rstrip()
            rel_path = os.path.relpath(full_path, install_dir)
            excluded = False
            for pattern in self.exclude_patterns:
                if re.search(pattern, rel_path):
                    excluded = True
            if not excluded:
                files.append(rel_path)
        return files

all_bundles = []

class Bundle:
    def __init__(self, label, filename, package_bundles):
        self.label = label
        self.filename = filename
        self.package_bundles = package_bundles
        global all_bundles
        all_bundles.append(self)

    def get_label(self):
        return self.label
    
    def create_bundle(self):
        files = []
        for package_bundle in self.package_bundles:
            files += package_bundle.get_files()
        files.sort()
        old_cwd = os.getcwd()
        tarfilename = self.filename + '.tar.gz'
        tar = tarfile.open(tarfilename, 'w:gz')
        os.chdir(self.package_bundles[0].get_install_dir())
        for filename in files:
            arcname = self.filename + '/' + filename
            tar.add(filename, arcname)
        print 'wrote', tarfilename

def list_bundles():
    for bundle in all_bundles:
        print bundle.get_label()

def create_bundle(label):
    found = False
    for bundle in all_bundles:
        if bundle.get_label() == label:
            found = True
            bundle.create_bundle()
    if not found:
        sys.stderr.write('Bundle "' + label +'" not found.\n')
        sys.exit(1)

        