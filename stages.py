#!/usr/bin/env python

from utils import *
from environment import *
import urllib, urlparse
import configuration
import string
import commands
from runtime import global_runtime_options
from root import default_parallel

build_msg_pre = "\x1B[07m"
build_msg_post = "\x1B[00m"

class Stage:
    def __init__(self, stage_name = 'generic_stage'):
        self.package = None
        self.stage_name = stage_name

    def get_name(self):
        return self.stage_name
        
    def set_name(self,name):
        self.stage_name = name
        
    def set_package(self, package):
        self.package = package
        self.precmd_opt = configuration.Option(self.package.root,
                self.package.get_name()+"/"+self.stage_name+ "_precmd",
                "",str,"prefix for build command",advanced=True)
        self.postcmd_opt = configuration.Option(self.package.root,
                self.package.get_name()+"/"+self.stage_name+ "_postcmd",
                "",str,"postfix for build command",advanced=True)

    def aux_stage(self):
        return None
        
    def _var(self, name):
        return self.package.get_var(name)

    def _vars(self):
        return self.package.get_vars()

    def _log(self):
        return os.path.join(self._var('log_dir'),self.stage_name)
    
    def _command_log(self):
        return os.path.join(self._var('log_dir'),self.stage_name)+".command"
    
    def _reset_command_log(self):
        if os.path.exists(self._command_log()):
            os.system("/bin/rm %s" % self._command_log())
            
    def build_method(self):
        pass

class Debug(Stage):
    def __init__(self, stage_name = 'debug'):
        Stage.__init__(self,'unpack')
    def build_method(self):
        print "vars ="
        keys = self._vars().keys()
        keys.sort()
        for key in keys:
            print key , ':' , self._vars()[key]
        sys.exit(0)
        
class Unpack(Stage):
    def __init__(self, archive_name=None, pattern=None, url=None):
        self.pattern = listify(pattern)
        if archive_name:
            self.archive_name = listify(archive_name)
        else:
            self.archive_name = [None]
            for i in range(1,len(self.pattern)):
                self.archive_name.append(None)
        self.url = listify(url)
        self.need_url = 0
        self.missing_archive = 0
        self.missing_message = ""
        Stage.__init__(self,'unpack')

    def _fetch_url(self):
        self.archive_name = []
        for url in self.url:
            pathname = urlparse.urlparse(url)[2]
            filename = os.path.basename(pathname)
            # special case for sourceforge-like urls with /download appended
            if filename == 'download':
                filename = os.path.basename(os.path.dirname(pathname))
            output_file = os.path.join(self._var("root.urlcache_dir"), filename)
            if not os.path.exists(output_file):
                command_message("fetching " + url)
                if not global_runtime_options.dry_run:
                    urllib.urlretrieve(url,output_file)
            self.archive_name.append(output_file)
                      
    def set_package(self, package):
        Stage.set_package(self, package)
        for i in range(0,len(self.archive_name)):
            if not self.archive_name[i]:
                if not self.pattern[i]:
                    self.pattern[i] = self.package.get_name() + "*"
                pattern = abs_rel_dir(self._var("root.archive_dir"),\
                    self.pattern[i])
                search_result = glob.glob(pattern)
                if len(search_result) == 1:
                    self.archive_name[i] = search_result[0]
                else:
                    if self.url != [None]:
                        self.need_url = 1
                    else:
                        self.missing_archive = 1
                        self.missing_message += \
                            '\nno match found for\n  "%s"' % pattern
       
    def build_method(self):
        self._reset_command_log()
        print "%s%s/%s%s" % \
              (build_msg_pre,self.package.name,self.stage_name,build_msg_post)
        if self.missing_archive:
            raise RuntimeError, 'Unable to find archive for ' + \
                      self.package.get_name() + ':' + \
                      self.missing_message
            
        if os.path.isdir(self._var('src_dir')):
            if self.package.is_precious():
                return
            system_or_die(('/bin/rm -rf %s' % self._var('src_dir')),command_log_file=self._command_log())
        if self.need_url:
            self._fetch_url()
        os.chdir(self._var('root.build_dir'))
        if not global_runtime_options.dry_run:
            if os.path.isdir('tmp'):
                system_or_die('/bin/rm -rf tmp',command_log_file=self._command_log())
            os.makedirs('tmp')
            os.chdir('tmp')
        if len(self.archive_name) > 1:
            command = "("
        else:
            command = ""
        count = 0
        for archive_name in self.archive_name:
            count += 1
            basic_cmd = self.precmd_opt.get() + 'tar xf'
            suffix = os.path.splitext(archive_name)[1]
            if suffix == self.precmd_opt.get() + '.zip':
                basic_cmd = 'unzip'
            if (suffix == '.tgz') or (suffix == '.gz'):
                basic_cmd = self.precmd_opt.get() + 'tar zxf'
            elif suffix == '.bz2':
                basic_cmd = self.precmd_opt.get() + 'tar jxf'
            if count > 1:
                command += " && "
            command += '%s %s' % \
                       (basic_cmd,
                        abs_rel_dir(self._var('root.archive_dir'),
                                    archive_name))
            command += self.postcmd_opt.get()
        if len(self.archive_name) > 1:
            command += ")"
        system_or_die(command, self._log(),command_log_file=self._command_log())
        output = glob.glob("*")
        if not global_runtime_options.dry_run:
            if len(output) == 0:
                raise RuntimeError, 'No output found from tar'
            elif len(output) > 1:
                os.makedirs(self._var('src_dir'))
        system_or_die('/bin/mv * %s' % self._var('src_dir'),command_log_file=self._command_log())
        os.chdir('..')
        system_or_die('/bin/rm -rf tmp',command_log_file=self._command_log())
        os.chdir(self._var("root.base_dir"))
        
class Cvs_checkout(Stage):
    def __init__(self, repository=None, module=None, branch=None, date=None):
        self.repository = repository
        self.module = module
        self.branch = branch
        self.date = date
        self.create_sc_tarball = Create_sc_tarball(self)
        Stage.__init__(self,'cvs_checkout')

    def set_package(self,package):
        Stage.set_package(self, package)
        if not self.module:
            self.module = self.package.get_name()
        self.git_option = configuration.Option(package.root,
                self.package.get_name()+"/use_git_repo",None,str,
                "checkout from git repository instead of cvs")
        if self.branch:
            default_git_branch = "origin/" + self.branch
        else:
            default_git_branch = None
        self.git_branch_option = configuration.Option(package.root,
                self.package.get_name()+"/git_branch",default_git_branch,str,
                "branch to use with git")
            
    def aux_stage(self):
        return self.create_sc_tarball
    
    def build_method(self):
        self._reset_command_log()
        print "%s%s/%s%s" % \
              (build_msg_pre,self.package.name,self.stage_name,build_msg_post)
        if os.path.isdir(self._var('src_dir')):
            if self.package.is_precious():
                return
            system_or_die('/bin/rm -rf %s' % self._var('src_dir'),command_log_file=self._command_log())
        os.chdir(self._var('root.build_dir'))
        if self.git_option.get() != 'None':
            self.git_checkout()
        else:
            self.checkout()
        os.chdir(self._var("root.base_dir"))

    def checkout(self):
        branch_arg = ''
        if self.repository:
            repository_arg = '-d %s' % self.repository
        else:
            repository_arg = ''
        if self.branch:
            branch_arg = '-r %s' % self.branch
        else:
            branch_arg = ''
        if self.date:
            date_arg = '-D %s' % self.date
        else:
            date_arg = ''
        system_or_die('%scvs %s checkout -d %s %s %s %s%s' % \
                      (self.precmd_opt.get(),
                      repository_arg,
                       self.package.get_name(),
                       branch_arg,
                       date_arg,
                       self.module,
                       self.postcmd_opt.get()),
                      self._log(),command_log_file=self._command_log())
    
    def git_checkout(self):
        system_or_die('%sgit clone -l %s %s' % \
            (self.precmd_opt.get(),self.git_option.get(), 
            self.package.get_name()),
            self._log(),command_log_file=self._command_log())
        if self.git_branch_option.get():
            if not global_runtime_options.dry_run:
                os.chdir(self.package.get_name())
            local_branch = self.git_branch_option.get().replace("origin/","")
            system_or_die('git checkout -b %s %s%s' % \
                (local_branch, self.git_branch_option.get(),self.postcmd_opt.get()),
                command_log_file=self._command_log())
            os.chdir(self._var("root.base_dir"))

class Svn_checkout(Stage):
    def __init__(self, url, module=None):
        self.url = url
        self.module = module
        self.create_sc_tarball = Create_sc_tarball(self)
        Stage.__init__(self,'svn_checkout')

    def set_package(self,package):
        Stage.set_package(self, package)
        self.git_option = configuration.Option(package.root,
                self.package.get_name()+"/use_git_svn",0,int,
                "checkout using git-svn of svn")
        self.git_svn_flags_option = configuration.Option(package.root,
                self.package.get_name()+"/git_svn_flags",
                "-t tags -T trunk -b branches",str,
                "flags to use with git-svn")
        default_git_svn_url = self.url
        url_pieces = self.url.split('/')
        end = len(url_pieces)
        if end > 1:
            if url_pieces[end-1] == 'trunk':
                default_git_svn_url = string.join(url_pieces[0:(end-1)],'/')
        if end > 2:
            if url_pieces[end-2] == 'branches':
                default_git_svn_url = string.join(url_pieces[0:(end-2)],'/')
        self.git_svn_url_option = configuration.Option(package.root,
                self.package.get_name()+"/git_svn_url",
                default_git_svn_url,str,
                "url to use with git-svn")
        if not self.module:
            self.module = self.package.get_name()
            
    def aux_stage(self):
        return self.create_sc_tarball
    
    def build_method(self):
        self._reset_command_log()
        print "%s%s/%s%s" % \
              (build_msg_pre,self.package.name,self.stage_name,build_msg_post)
        if os.path.isdir(self._var('src_dir')):
            if self.package.is_precious():
                return
            system_or_die('/bin/rm -rf %s' % self._var('src_dir'),
                command_log_file=self._command_log())
        os.chdir(self._var('root.build_dir'))
        self.checkout()
        os.chdir(self._var("root.base_dir"))

    def checkout(self):
        if self.git_option.get():
            system_or_die('%sgit svn clone %s %s %s' % 
                (self.precmd_opt.get(),self.git_svn_flags_option.get(),
                self.git_svn_url_option.get(),self.module),
                          self._log(),command_log_file=self._command_log())
            system_or_die('cd %s; git checkout %s%s' %
                (self.module, self.module,self.postcmd_opt.get()),
                    command_log_file=self._command_log())
        else:
            system_or_die('%ssvn checkout %s %s%s' % 
                (self.precmd_opt.get(),self.url,self.module,
                self.postcmd_opt.get()),
                          self._log(),command_log_file=self._command_log())
        if self.module != self.package.get_name():
            system_or_die("/bin/mv %s %s" % (self.module, self.package.get_name()),command_log_file=self._command_log())


class Git_clone(Stage):
    def __init__(self, url, branch = "master", anon_url = None, 
                 cvs_url = None):
        self.url = url
        self.branch = branch
        self.anon_url = anon_url
        self.cvs_url = cvs_url
        self.module = None
        Stage.__init__(self,'git-clone')

    def set_package(self,package):
        Stage.set_package(self, package)
        if not self.module:
            self.module = self.package.get_name()

    def build_method(self):
        self._reset_command_log()
        print "%s%s/%s%s" % \
              (build_msg_pre,self.package.name,self.stage_name,build_msg_post)
        if os.path.isdir(self._var('src_dir')):
            if self.package.is_precious():
                return
            system_or_die('/bin/rm -rf %s' % self._var('src_dir'),command_log_file=self._command_log())
        os.chdir(self._var('root.build_dir'))
        cmd = 'git --version'
        (status,output) = commands.getstatusoutput(cmd)
        if status:
            ### no git found
            if self.cvs_url:
                system_or_die('%scvs -d %s checkout %s -d %s' % \
                        (self.precmd_opt.get(), self.cvs_url, self.branch,
                         self.package.get_name()),
                        self._log(),command_log_file=self._command_log())
            else:
                raise RuntimeError, \
                    "git not found and no fallback cvs_url available"
        else:
            ### git found
            try:
                system_or_die('%sgit clone --no-checkout -l %s %s' % \
                    (self.precmd_opt.get(),self.url, self.package.get_name()),
                    self._log(),command_log_file=self._command_log(),
                    raise_exception=True)
            except:
                if self.anon_url:
                    system_or_die('%sgit clone --no-checkout -l %s %s' % \
                    (self.precmd_opt.get(),self.anon_url,
                     self.package.get_name()),
                     self._log(),command_log_file=self._command_log())
                else:
                    raise sys.exc_type, sys.exc_value
            if not global_runtime_options.dry_run:
                os.chdir(self.package.get_name())
            if self.branch != "master":
                system_or_die('git branch %s origin/%s' % (self.branch, self.branch))
                # extra checkout to work around git bug in at least 1.6.0.4 (jaunty)
                system_or_die('git checkout ; true')
            system_or_die('git checkout %s%s' % \
                (self.branch,self.postcmd_opt.get()),command_log_file=self._command_log())
        os.chdir(self._var("root.base_dir"))

class Create_sc_tarball(Stage):
    def __init__(self, checkout):
        self.checkout = checkout 
        Stage.__init__(self,'create_sc_tarball')

    def build_method(self):
        self._reset_command_log()
        print "%s%s/%s%s" % \
              (build_msg_pre,self.checkout.package.name,
               self.stage_name,build_msg_post)
        tmpdir = self.checkout._var('src_dir') + '-tarballtmp'
        if os.path.isdir(tmpdir):
            system_or_die('/bin/rm -rf %s' % tmpdir,command_log_file=self._command_log())
        os.makedirs(tmpdir)
        os.chdir(tmpdir)
        self.checkout.checkout()
        system_or_die('tar zcf %s.tar.gz %s' % \
                      (self.checkout.package.get_name(),
                       self.checkout.package.get_name()),command_log_file=self._command_log())
        system_or_die('/bin/mv %s.tar.gz %s' % \
                      (self.checkout.package.get_name(),
                       self.checkout._var("root.archive_dir")),command_log_file=self._command_log())
        system_or_die('/bin/rm -rf %s' % tmpdir,command_log_file=self._command_log())
        os.chdir(self.checkout._var("root.base_dir"))

class Bootstrap(Stage):
    def __init__(self, command=None):
        if command:
            self.command = command
        else:
            self.command = "./bootstrap"
        Stage.__init__(self,'bootstrap')
        
    def build_method(self):
        self._reset_command_log()
        print "%s%s/%s%s" % \
              (build_msg_pre,self.package.name,self.stage_name,build_msg_post)
        os.chdir(self._var("build_dir"))
        system_or_die("%s%s%s" % 
            (self.precmd_opt.get(),self.command % self._vars(), self.postcmd_opt.get()),
            self._log(),command_log_file=self._command_log())
        os.chdir(self._var("root.base_dir"))

class Configure(Stage):
    def __init__(self, extra_args=None, all_args=None):
        self.extra_args = extra_args
        self.all_args = all_args
        Stage.__init__(self,'configure')
            
    def build_method(self):
        self._reset_command_log()
        print "%s%s/%s%s" % \
              (build_msg_pre,self.package.name,self.stage_name,build_msg_post)
        if not os.path.isdir(self._var("build_dir")):
            os.makedirs(self._var("build_dir"))
        os.chdir(self._var("build_dir"))
        if self.all_args:
            args = self.all_args
        else:
            args = "--prefix=%(root.install_dir)s"
            if self.extra_args:
                args += " " + self.extra_args
        system_or_die((self.precmd_opt.get() + '%(src_dir)s/configure' +
            ' ' + args +self.postcmd_opt.get()) % self._vars(),
                      self._log(),command_log_file=self._command_log())
        os.chdir(self._var("root.base_dir"))

class Cmake(Stage):
    def __init__(self, extra_args=None, all_args=None):
        self.extra_args = extra_args
        self.all_args = all_args
        Stage.__init__(self,'cmake')
            
    def build_method(self):
        self._reset_command_log()
        print "%s%s/%s%s" % \
              (build_msg_pre,self.package.name,self.stage_name,build_msg_post)
        if not os.path.isdir(self._var("build_dir")):
            os.makedirs(self._var("build_dir"))
        os.chdir(self._var("build_dir"))
        if self.all_args:
            args = self.all_args
        else:
            args = "-DCMAKE_INSTALL_PREFIX:PATH=%(root.install_dir)s"
            if self.extra_args:
                args += " " + self.extra_args
        system_or_die((self.precmd_opt.get() + 'cmake' + ' ' + args +
            ' ' + self._var("src_dir") + self.postcmd_opt.get()) \
            % self._vars(),
                      self._log(),command_log_file=self._command_log())
        os.chdir(self._var("root.base_dir"))

class Make(Stage):
    def __init__(self, parallel=True):
        Stage.__init__(self,'make')
        self.parallel = parallel
        
    def set_package(self, package):
        Stage.set_package(self, package)
        if self.parallel:
            self.use_custom_parallel_opt = configuration.Option(self.package.root,
                    self.package.get_name()+"/"+self.stage_name+ "_use_custom_parallel",
                    False, bool,
                    "use custom number of parallel processes for build command",
                    advanced=False)
            self.custom_parallel_opt = configuration.Option(self.package.root,
                    self.package.get_name()+"/"+self.stage_name+ "_custom_parallel",
                    default_parallel.get(), int,
                    "custom number of parallel processes for build command",
                    advanced=False)

    def build_method(self):
        self._reset_command_log()
        print "%s%s/%s%s" % \
              (build_msg_pre,self.package.name,self.stage_name,build_msg_post)
        os.chdir(self._var("build_dir"))
        if self.parallel:
            if self.use_custom_parallel_opt.get():
                parallel_flag = ' -j %d' % self.custom_parallel_opt.get()
            else:
                parallel_flag = ' -j %d' % default_parallel.get()
        else:
            parallel_flag = ''
        system_or_die(self.precmd_opt.get() + 'make' + parallel_flag +
            self.postcmd_opt.get(),
            self._log(),command_log_file=self._command_log())
        os.chdir(self._var("root.base_dir"))

class Ninja(Stage):
    def __init__(self):
        Stage.__init__(self,'ninja')

    def build_method(self):
        self._reset_command_log()
        print "%s%s/%s%s" % \
              (build_msg_pre,self.package.name,self.stage_name,build_msg_post)
        os.chdir(self._var("build_dir"))
        system_or_die(self.precmd_opt.get() + 'ninja' + self.postcmd_opt.get(),
            self._log(),command_log_file=self._command_log())
        os.chdir(self._var("root.base_dir"))
        
class Qmake(Stage):
    def __init__(self, pro_file="*.pro"):
        self.pro_file = pro_file
        Stage.__init__(self,'qmake')
        
    def build_method(self):
        self._reset_command_log()
        print "%s%s/%s%s" % \
              (build_msg_pre,self.package.name,self.stage_name,build_msg_post)
        os.chdir(self._var("build_dir"))
        system_or_die('%sqmake %s%s' % (self.precmd_opt.get(),self.pro_file,
                self.postcmd_opt.get()),self._log(),command_log_file=self._command_log())
        os.chdir(self._var("root.base_dir"))

class Install(Stage):
    def __init__(self):
        Stage.__init__(self,'install')
        
    def build_method(self):
        self._reset_command_log()
        print "%s%s/%s%s" % \
              (build_msg_pre,self.package.name,self.stage_name,build_msg_post)
        os.chdir(self._var("build_dir"))
        changed_files = Changed_files(self._var("root.install_dir"))
        system_or_die(self.precmd_opt.get()+'make install'+
            self.postcmd_opt.get(),self._log(),command_log_file=self._command_log())
        changed_files.end()
        changed_files.dump_all(self._log()+".changed_files")
        os.chdir(self._var("root.base_dir"))

class Ninja_install(Stage):
    def __init__(self):
        Stage.__init__(self,'ninja_install')
        
    def build_method(self):
        self._reset_command_log()
        print "%s%s/%s%s" % \
              (build_msg_pre,self.package.name,self.stage_name,build_msg_post)
        os.chdir(self._var("build_dir"))
        changed_files = Changed_files(self._var("root.install_dir"))
        system_or_die(self.precmd_opt.get()+'ninja install'+
            self.postcmd_opt.get(),self._log(),command_log_file=self._command_log())
        changed_files.end()
        changed_files.dump_all(self._log()+".changed_files")
        os.chdir(self._var("root.base_dir"))

class Py_install(Stage):
    def __init__(self, extra_args=None):
        if extra_args:
            self.extra_args = extra_args
        else:
            self.extra_args = ''
        Stage.__init__(self,'py_install')
            
    def build_method(self):
        self._reset_command_log()
        print "%s%s/%s%s" % \
              (build_msg_pre,self.package.name,self.stage_name,build_msg_post)
        if not global_runtime_options.dry_run:
            os.chdir(self._var("build_dir"))
        if self.extra_args:
            extra_args = self.extra_args
        else:
            extra_args = ''
        changed_files = Changed_files(self._var("root.install_dir"))
        prefix_arg = ''
        (status,python_prefix) = commands.getstatusoutput('python -c "import distutils.sysconfig;import sys;sys.stdout.write(distutils.sysconfig.PREFIX)"')
        if python_prefix == self.package.get_var('root.install_dir'):
            pass
        else:
            prefix_arg = "--prefix %(root.install_dir)s"
        system_or_die((self.precmd_opt.get() + 'python setup.py install ' + prefix_arg + \
                       extra_args+ self.postcmd_opt.get()) % self.package.get_vars(),
                      self._log(),command_log_file=self._command_log())
        changed_files.end()
        changed_files.dump_all(self._log()+".changed_files")
        os.chdir(self._var("root.base_dir"))

class Build_command(Stage):
    def __init__(self, stage_name, command, record_install=0):
        self.command = command
        self.record_install = record_install
        Stage.__init__(self,stage_name)
        
    def build_method(self):
        self._reset_command_log()
        print "%s%s/%s%s" % \
              (build_msg_pre,self.package.name,self.stage_name,build_msg_post)
        if not os.path.isdir(self._var("build_dir")):
            os.makedirs(self._var("build_dir"))
        os.chdir(self._var("build_dir"))
        if self.record_install:
            changed_files = Changed_files(self._var("root.install_dir"))
        system_or_die((self.precmd_opt.get()+self.command+
            self.postcmd_opt.get()) % self.package.get_vars(),
                      self._log(),command_log_file=self._command_log())
        if self.record_install:
            changed_files.end()
            changed_files.dump_all(self._log()+".changed_files")
        os.chdir(self._var("root.base_dir"))

class _Persistent_value:
    def __init__(self,filename):
        self.filename = filename
        self.have_value = False
        self.value = None
        
    def set(self,value):
        if not os.path.exists(os.path.dirname(self.filename)):
            os.makedirs(os.path.dirname(self.filename))
        f = open(self.filename,'w')
        f.write('%s\n' % value)
        f.close()
        self.value = value
        self.have_value = True
        
    def get(self):
        if self.have_value:
            retval = self.value
        else:
            if os.path.exists(self.filename):
                f = open(self.filename,'r')
                retval = f.readline()
                f.close()
                # remove trailing newline
                retval = retval[0:len(retval)-1]
            else:
                retval = "None"
        return retval

class Add_package_vars(Stage):
    def __init__(self,vars,functions):
        if (type(vars) == list) or (type(vars) == tuple):
            self.vars = vars
        else:
            self.vars = [vars]
        if (type(functions) == list) or (type(functions) == tuple):
            self.functions = functions
        else:
            self.functions = [functions]
        if len(self.vars) != len(self.functions):
            raise RuntimeError, "Add_package_vars: len(vars) must be same as len(functions)"
        self.values = []
        Stage.__init__(self,'Add_package_vars')
    
    def set_package(self, package):
        Stage.set_package(self,package)
        for i in range(0,len(self.vars)):
            value_db_file = os.path.join(self.package.get_var('tag_dir'),
                '%s.value' % self.vars[i])
            self.values.append(_Persistent_value(value_db_file))
            self.package.new_var(self.vars[i],self.values[i].get())
    
    def build_method(self):
        self._reset_command_log()            
        for i in range(0,len(self.vars)):
            if global_runtime_options.dry_run:
                try:
                    self.values[i].set(self.functions[i](self.package.get_vars()))
                except:
                    self.values[i].set('DRY_RUN_EXCEPTION')
            else:
                self.values[i].set(self.functions[i](self.package.get_vars()))
            self.package.get_vars()[self.vars[i]] = self.values[i].get()

class Modify_environment(Stage):
    def __init__(self,var,item,action=ENV_PREPEND,delimiter=':'):
        Stage.__init__(self,'modify_'+var.lower())
        self.var = var
        self.item = item
        self.action = action
        self.delimiter = delimiter
        
    def build_method(self):
        self._reset_command_log()
        print "%s%s/%s%s" % \
              (build_msg_pre,self.package.name,self.stage_name,build_msg_post)
        if callable(self.item):
            real_item = apply(self.item) % self._vars()
        else:
            real_item = self.item % self._vars()
        env_node = Environment_mod_node(self.package.root,self.package.get_name(),
            self.var,real_item,self.action,self.delimiter)
        env_node.create_files()
