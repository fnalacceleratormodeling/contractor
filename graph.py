#!/usr/bin/env python

class Graph:
    def __init__(self):
        self._nodes = {}
        self._target_names = []
        self._default_name = None
        self._explicit_default = 0
    
    def non_empty(self):
        return len(self._nodes.keys())
        
    def get_nodes(self):
        return self._nodes
    
    def get_node_names(self):
        return self._nodes.keys()
    
    def get_node(self,name):
        return self._nodes[name]

    def has_node(self,name):
        return self._nodes.has_key(name)

    def get_target_names(self):
        return self._target_names
    
    def get_default(self):
        return self._nodes[self._default_name]
    
    def get_default_name(self):
        return self._default_name
    
    def add_node(self,node,target=0,default=0):
        self._nodes[node.get_name()] = node
        if target:
            self.make_target(node)
        if self._explicit_default == 0:
            self.make_default(node)
        if default:
            self._explicit_default = 1
            self.make_default(node)
    
    def make_target(self, node):
        self._target_names.append(node.get_name())
        
    def make_default(self, node):
        self._default_name = node.get_name()
    
    def in_graph(self, node):
        retval = 0
        if self._nodes.has_key(node.get_name()):
            if self._nodes[node.get_name()] == node:
                retval = 1
        return retval